const http = require('http')
const https = require('https')
const axios = require('axios')
const MongoClient = require('mongodb').MongoClient
const assert = require('assert')

module.exports = class HttpCacheToMongo {
  constructor (obj) {
    const {
      apiKeyName,
      apiKeyValue,
      dbName,
      collectionName,
      connectionUrl,
      paramsString
    } = obj

    this.apiKeyName = apiKeyName
    this.apiKeyValue = apiKeyValue
    this.dbName = dbName
    this.collectionName = collectionName
    this.connectionUrl = connectionUrl
    this.paramsString = paramsString

    return new Promise(resolve => {
      MongoClient.connect(this.connectionUrl, (err, client) => {
        assert.strictEqual(null, err)
        this.db = client.db(this.dbName)
        this.collection = this.db.collection(this.collectionName)
        resolve(this)
      })
    })
  }

  insertDocument (data, url, callback) {
    const documentByUrl = {
      url,
      data: data
    }

    this.collection.insertOne(documentByUrl, (err, result) => {
      assert.strictEqual(err, null)
      assert.strictEqual(1, result.result.n)
      assert.strictEqual(1, result.ops.length)
      callback(result.ops)
    })
  }

  get (url, responseType = 'json', binaryFileName = '', noParams = false) {
    let absUrl

    noParams
      ? absUrl = new URL(`${url}`)
      : absUrl = new URL(`${url}?${this.apiKeyName}=${this.apiKeyValue}${this.paramsString}`)

    return new Promise((resolve, reject) => {
      this.collection.find({
        url: absUrl.href
      })
        .toArray((docs) => {
          // if the data for this url already exists, return it.
          if (docs.length > 0) {
            responseType === 'stream' && binaryFileName
              ? resolve(docs[0].data.buffer)
              : resolve(docs)
          } else { // else make a call to the webservice and store it in mongodb
            if (responseType === 'stream' && binaryFileName) {
              let data = []
              let httpChosen

              absUrl.protocol === 'https:'
                ? httpChosen = https
                : httpChosen = http
              httpChosen.get(absUrl.href, response => {
                // push each chunk to build an array of chunks
                response.on('data', chunk => {
                  data.push(chunk)
                })

                response.on('end', () => {
                  data = Buffer.concat(data) // do something with data
                  this.insertDocument(data, url, docs => {
                    // in this code, .data is the buffer
                    resolve(docs[0].data)
                  })
                })
              })
            } else {
              axios
                .get(absUrl.href)
                .then(response => {
                  this.insertDocument(response.data, url, docs => {
                    resolve(docs)
                  })
                })
                .catch((error) => {
                  console.log(error)
                  reject(error)
                })
            }
          }
        })
    })
  }
}
